//
//  AppDelegate.swift
//  Rennspur
//
//  Created by Tim Prangel on 21.02.17.
//  Copyright © 2017 Tim Prangel. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        
        
        //        NSLog("query URL %@", url.query ?? "-")
        //        NSLog("host URL %@", url.host ?? "-")
        //        NSLog("path URL %@", url.path)
        
        let vars: [String]? = url.query?.components(separatedBy: "&")
        var params: [String: String] = [String: String]()
        
        for a in vars! {
            let comps: [String] = a.components(separatedBy: "=")
            params[comps[0]] = comps[1]
        }
        
        //        NSLog("%@", params["host"]!)
        
        let settings = Settings.instance
        settings.host = params["host"]!
        settings.hash1 = params["hash"]!
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "settingsUpdated"), object: nil)
        
        return true
        
        
        
    }
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

